#include <iostream>
using namespace std;

class Student{
public:
    string m_jmeno;
    int m_rok_narozeni;
    float m_znamka;
    bool m_stredni_skola;

    Student(){
        m_jmeno = "Bez jmena";
        m_rok_narozeni = 1900;
        m_znamka = 0.0;
        m_stredni_skola = false;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void setJmeno (string nove_jmeno){
        m_jmeno = nove_jmeno;

    }

    int getRokNarozeni(){
        return m_rok_narozeni;
    }

    void setRokNarozeni(int novy_rok_narozeni) {
        m_rok_narozeni = novy_rok_narozeni;
    }

    void setZnamka (float nova_znamka){
        m_znamka = nova_znamka;
    }

    void printInfo (){
        cout << "Jsem student " << m_jmeno;
        cout << " a narodila jsem se v roce " << m_rok_narozeni << endl;
        cout<< "Prumerna znamka studia je: " << m_znamka;
        if (m_stredni_skola == true){
            cout << " a byla jsi prijata na stredni skolu bez prijimacich zkousek.";
        } else {
            cout << " a na prijeti bez prijimacich zkousek nevyhovujes parametrum.";
        }
    }

    float getZnamka (){
        return m_znamka;
        if (m_znamka>5){
            m_stredni_skola = false;
        } else {
            if (m_znamka < 2.5) {
                m_stredni_skola = true;
            }
        }
    }

};


int main() {
    Student* s1 = new Student();
    s1->setJmeno("Misa");
    s1 ->setRokNarozeni(2000);
    s1 ->setZnamka(7.00);
    s1 ->printInfo();


    return 0;
}